import React, { Component } from 'react'
import { render } from 'react-dom'
import App from '../app'

import './style/main.scss'

render(
  <App />,
  document.querySelector('#app')
)
