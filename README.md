# haskell-react

Haskell/Scotty boilerplate with React frontend.

## Requirements

Tested with the following versions:

```
stack 1.7.1
ghc 8.4.3
npm 5.6.0
node 9.11.1
```

### Stack Installation

```bash
curl -sSL https://get.haskellstack.org/ | sh
```

## Building and Running

```
stack build
stack exec haskell-react-exe
```

Or with `make`

```
make start
```
