{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Lib
    ( home
    , login
    , post
    ) where

import           Web.Scotty (ScottyM, json, get, html, middleware)
import           Data.Aeson (ToJSON, encode)
import           Data.ByteString.Lazy.UTF8 (toString)
import           Data.Text.Lazy (pack)
import           Data.List.Utils (replace)
import           GHC.Generics

{-
  TODO: Use Blaze HTML templating?
  Note: Protects against injection better by escaping the encoded state JSON string.
        Like: `JSON.stringify(state).replace(/</g, '\\u003c')`
        Reference: https://redux.js.org/recipes/serverrendering
-}
home :: ScottyM ()
home = get "/" $
  html $ pack template
    where title = "Haskell React Starter"
          initialState = toString (encode $ State (Session 123 "<script>hacky stuff</script>"))
          template = "<!DOCTYPE html> \
                    \ <html lang=\"en\"> \
                    \ <head> \
                    \   <meta charset=\"UTF-8\"> \
                    \   <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
                    \   <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"> \
                    \   <title>" ++ title ++ "</title> \
                    \   <link rel=\"stylesheet\" href=\"/dist/bundle.css\"> \
                    \   <script> \
                    \     window.__INITIAL_STATE__ = " ++ replace "<" "\\u003c" initialState ++ "; \
                    \   </script> \
                    \ </head> \
                    \ <body> \
                    \   <div id=\"app\"></div> \
                    \   <script src=\"/dist/bundle.js\"></script> \
                    \ </body> \
                    \ </html>"

login :: ScottyM ()
login = get "/login" $ html "login"

{-
  Example data structure to demonstrate JSON serialization
-}
data Post = Post
  { post_id    :: Int
  , post_title :: String } deriving Generic

instance ToJSON Post

data State = State
  { session :: Session } deriving Generic

instance ToJSON State

data Session = Session
  { id :: Int
  , some_dangerous_string :: String } deriving Generic

instance ToJSON Session

post :: ScottyM()
post = get "/post" $ json $ Post 1 "Yello world"
