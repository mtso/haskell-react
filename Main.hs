{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Main where

import           Control.Applicative                  ((<$>))
import           Control.Monad                        (join)
import           Data.Maybe                           (fromMaybe)
import           System.Environment                   (lookupEnv, getEnv)
import           Text.Read                            (readMaybe)
import           Web.Scotty (ScottyM, get, html, json, middleware, scotty)
-- import           GHC.Generics

import           Network.Wai.Middleware.RequestLogger (logStdoutDev)
import           Network.Wai.Middleware.Static        (addBase, noDots,
                                                       staticPolicy, (>->))

import Lib (home, login, post)

main :: IO ()
main = do
  port <- fromMaybe 3000
    . join
    . fmap readMaybe <$> lookupEnv "PORT"
  scotty port $ do
    middleware $ staticPolicy (noDots >-> addBase "public")
    home >> login >> post
