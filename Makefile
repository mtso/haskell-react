build:
	stack build

build-client:
	npm install && npm run build

start: build build-client
	stack exec haskell-react-exe

start-dev: build
	stack exec haskell-react-exe
